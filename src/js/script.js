// ________________________________________________________________ //

let windowUrl = window.location.href;
const buildingDetails = {};

function fetchData() {
  // basic details
  buildingDetails["title"] = document.querySelector("#bd3 > span").textContent;
  buildingDetails["url"] = windowUrl;

  // getting project key
  let project = {};
  project["name"] = document.querySelector("#propUnitDesc").textContent.trim();
  project["url"] = windowUrl;
  buildingDetails["project"] = project;

  // get description
  FetchImages(buildingDetails);
  fetchPropertyDetails();
  getProjectInfo();
  getDescription();
  getPropertyDetails();
  fetchBuilderDetails();

  setTimeout(() => {
    chrome.storage.sync.set(
      { buildingDetails: JSON.stringify(buildingDetails) },
      () => {
        console.log("Settings saved");
      }
    );
  }, 7000);
}

function getDescription() {
  let desc = document.querySelector(
    "#propDetailDescriptionId > div.propDescription.clearfix > div > div:nth-child(1)"
  );
  buildingDetails["description"] = desc ? desc.textContent.trim() : "";
}

function getPropertyDetails() {
  let propDetailDescriptionIdKeys = document.querySelectorAll(
    "#propDetailDescriptionId > div.propDescription.clearfix > div > .p_infoRow .p_title"
  );

  let propDetailDescriptionIdValues = document.querySelectorAll(
    "#propDetailDescriptionId > div.propDescription.clearfix > div > .p_infoRow .p_value"
  );

  // projectDetails
  let projectDetails = {};
  propDetailDescriptionIdKeys.forEach((e, i) => {
    let key = e.textContent.trim();
    let value = propDetailDescriptionIdValues[i].textContent.trim();

    value = value.split(" ").map((e) => e.trim());

    projectDetails[key] = value.join("");
  });
  buildingDetails["projectDetails"] = projectDetails;
}

function getProjectInfo() {
  let projectInfoKeys = document.querySelectorAll(
    ".propInfoBlockInn .p_infoColumn .p_title"
  );
  let projectInfoValues = document.querySelectorAll(
    ".propInfoBlockInn .p_infoColumn .p_value"
  );

  projectInfoKeys.forEach((e, i) => {
    let key = e.textContent.trim();
    let value = projectInfoValues[i].textContent.trim();

    if (i == 0) {
      buildingDetails[key.toLowerCase()] =
        value.trim().split("\n")[0].trim() + " " + typeof value == "string"
          ? value.trim().split("\n")[3].trim()
          : value;
    } else {
      buildingDetails[key] = value;
    }
  });

  buildingDetails["developer"] = {
    name: document.querySelector("#propUnitDesc").textContent.trim(),
    url: document
      .querySelector(
        "#thirdFoldDisplay > div:nth-child(1) > div.p_value > span > a"
      )
      .getAttribute("href"),
  };
}

function fetchBuilderDetails() {
  let builder = {};

  builder[" name"] = document.querySelector(
    "#agentDetailTabId > div.agentWrap > div.agentInfo > div > div.agentName"
  ).textContent;

  builder["location"] = document
    .querySelector(
      "#agentDetailTabId > div.agentWrap > div.agentInfo > div > p"
    )
    .textContent.split(":")[1]
    .trim();
  builder["image"] = document
    .querySelector(
      "#agentDetailTabId > div.agentWrap > div.agentHead > div.agentPhotoWrap > div > img"
    )
    .getAttribute("src");

  buildingDetails["builderDetails"] = builder;
}

const FetchImages = (buildingDetails) => {
  let scriptURL = "https://www.magicbricks.com";

  let imgsbase = document.querySelector(
    "#propertyDetailTabId > div.whiteBgBlock > div.propOverView > div.propImageBlock img"
  );

  imgsbase = imgsbase ? imgsbase.getAttribute("onclick").split("'")[1] : "";

  scriptURL =
    scriptURL +
    imgsbase +
    "&fromPage=detail&nonprimemember=Y&primeSource=MBPrime_PropVerification_CTA-Linux&category=S";

  let imgFrame = document.createElement("iframe");
  imgFrame.src = scriptURL;
  //   imgFrame.style.display = "none";
  imgFrame.id = "customPicFrame";
  document.body.append(imgFrame);

  setTimeout(() => {
    let images = [];
    let doc = document.getElementById("customPicFrame").contentWindow.document;
    let allimages = doc.querySelectorAll("#propertyImageSlider ul li div img");

    allimages.forEach((e, i) => {
      images.push({
        name: e.getAttribute("alt") ? e.getAttribute("alt") : "Image " + i + 1,
        url: e.getAttribute("data-src"),
      });
    });

    buildingDetails["images"] = images;
  }, 2000);
};

function fetchPropertyDetails() {
  // property Details
  let propertyDetails = {};

  let rating = document.querySelector(
    "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsHead > div.detailsHeadL > div.proj-rating > div:nth-child(1) > div.proj-rating__row > span.proj-rating__number"
  );
  propertyDetails["stars"] = rating.textContent.trim();
  document.querySelector(
    "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsRow > div.column.col_1 > div.detailsVal > div.semiBold"
  );

  propertyDetails["price"] = {
    total: document.querySelector(
      "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsRow > div.column.col_1 > div.detailsVal > div.semiBold"
    ).textContent,
    perSqft: document.querySelector(
      "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsRow > div.column.col_1 > div.detailsVal > div.openSans_regular.fsize_12"
    ).textContent,
  };

  propertyDetails["Configuration"] = document.querySelector(
    "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsRow > div.column.col_2 > div.detailsVal"
  ).textContent;

  propertyDetails["Towers & Unit Details"] = document.querySelector(
    "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsRow > div.column.col_2 > div.detailsVal"
  ).textContent;

  propertyDetails["Under Construction"] = document
    .querySelector(
      "#projectDetailTabId > div.projDetails > div.detailsCont > section.detailsInfo > div.detailsRow > div.column.col_4 > div.detailsVal"
    )
    .textContent.trim();

  buildingDetails["propertyDetails"] = propertyDetails;
}

function fetchLoaclityDetails() {
  let localityDetails = {};

  // let basedOn = document.querySelector(
  //   "#rating > div.revComBlock > div.rateBlock > div.rateBlockL > div.eveRatingRef"
  // ).textContent;

  let ratings = document.querySelector(
    "#rating > div.revComBlock > div.rateBlock > div.rateBlockL > div.eveRating > div"
  ).textContent;

  localityDetails["basedOn"] = basedOn ? basedOn : "";
  localityDetails["stars"] = ratings ? ratings : "";

  // get stuffsssss

  let env = {
    neighbourhood: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.environment > div.rateRate > ul > li:nth-child(1) > div.rStar > span.plus1"
    ).length,
    clearnliness: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.environment > div.rateRate > ul > li:nth-child(4) > div.rStar > span.plus1"
    ).length,
    safty: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.environment > div.rateRate > ul > li:nth-child(3) > div.rStar > span.plus1"
    ).length,
    roads: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.environment > div.rateRate > ul > li:nth-child(2) > div.rStar > span.plus1"
    ).length,
  };

  let placesOfInt = {
    schools: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.placesofinterest > div.rateRate > ul > li:nth-child(1) > div.rStar > span.plus1"
    ).length,
    restaurents: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.placesofinterest > div.rateRate > ul > li:nth-child(2) > div.rStar > span.plus1"
    ).length,
    hospitals: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.placesofinterest > div.rateRate > ul > li:nth-child(3) > div.rStar > span.plus1"
    ).length,
    market: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.placesofinterest > div.rateRate > ul > li:nth-child(4) > div.rStar > span.plus1"
    ).length,
  };

  let commuting = {
    connectivity: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.commuting > div.rateRate > ul > li:nth-child(3) > div.rStar > span.plus1"
    ).length,

    publichTransport: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.commuting > div.rateRate > ul > li:nth-child(1) > div.rStar > span.plus1"
    ).length,

    parking: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.commuting > div.rateRate > ul > li:nth-child(2) > div.rStar > span.plus1"
    ).length,

    traffic: document.querySelectorAll(
      "#rating > div.revComBlock > div.rateBlock > div.rateBlockR > div.rateBlocks.commuting > div.rateRate > ul > li:nth-child(2) > div.rStar > span.plus1"
    ).length,
  };

  localityDetails["enviroment"] = env;
  localityDetails["placesOfIntrests"] = placesOfInt;
  localityDetails["commuting"] = commuting;

  let mainNode = document.querySelectorAll("#detailsHighLight-id > ul > li");
  let highlights = [];
  let localAdvantages = [];

  let high = false;
  mainNode.forEach((e) => {
    if (e.childElementCount === 1) high = true;

    if (!high) highlights.push(e.textContent);
    else localAdvantages.push(e.textContent);
  });

  localAdvantages[0] = localAdvantages[0].split(":")[1];

  localityDetails["Highlights"] = highlights;
  localityDetails["localAdvantages"] = localAdvantages;

  buildingDetails["localityDetails"] = localityDetails;
}

if (windowUrl.includes("https://www.magicbricks.com/propertyDetails/")) {
  fetchData();
}
